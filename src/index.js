import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import KomponenTugas from './components/KomponenTugas';
import * as serviceWorker from './serviceWorker';

//JSX
const element_jsx = <h2>1. Element JSX</h2>;

//React
const element_react = React.createElement('p', {},
 '2. Ini menggunakan react.');

//atribut child dan tanpa child
const nama = "Wayan"
const tl = 1997
const ekspresi_js = (
  <div>
    <h1>Hai, {nama}.</h1>
    <h2>Kamu lahir pada tahun {tl}</h2>
    <h3>Jadi umur kamu {2020-tl}</h3>
  </div>
);

//komponen

ReactDOM.render(
  <React.StrictMode>
    <App/>
    <KomponenTugas/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
